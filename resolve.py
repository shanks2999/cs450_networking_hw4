"""
resolve.py: a recursive resolver built using dnspython
"""

import argparse

import dns.message
import dns.name
import dns.query
import dns.rdata
import dns.rdataclass
import dns.rdatatype

from functools import lru_cache

total_calls_performed = int = 0
whitelisted_cache = set()
blacklisted_cache = set()
tld_cache = dict()
canonical_resolution = {}
FORMATS = (("CNAME", "{alias} is an alias for {name}"),
           ("A", "{name} has address {address}"),
           ("AAAA", "{name} has IPv6 address {address}"),
           ("MX", "{name} mail is handled by {preference} {exchange}"))

# current as of 23 February 2017
ROOT_SERVERS = ("198.41.0.4",
                "192.228.79.201",
                "192.33.4.12",
                "199.7.91.13",
                "192.203.230.10",
                "192.5.5.241",
                "192.112.36.4",
                "198.97.190.53",
                "192.36.148.17",
                "192.58.128.30",
                "193.0.14.129",
                "199.7.83.42",
                "202.12.27.33")


def collect_results(name: str) -> dict:
    """
    This function parses final answers into the proper data structure that
    print_results requires. The main work is done within the `lookup` function.
    """
    full_response = {}
    target_name = dns.name.from_text(name)
    # lookup CNAME
    response = lookup(target_name, dns.rdatatype.CNAME)
    cnames = []
    for answers in response.answer:
        for answer in answers:
            cnames.append({"name": answer, "alias": name})
    # lookup A
    response = lookup(target_name, dns.rdatatype.A)
    arecords = []
    for answers in response.answer:
        a_name = answers.name
        for answer in answers:
            if answer.rdtype == 1:  # A record
                arecords.append({"name": a_name, "address": str(answer)})
    # lookup AAAA
    response = lookup(target_name, dns.rdatatype.AAAA)
    aaaarecords = []
    for answers in response.answer:
        aaaa_name = answers.name
        for answer in answers:
            if answer.rdtype == 28:  # AAAA record
                aaaarecords.append({"name": aaaa_name, "address": str(answer)})
    # lookup MX
    response = lookup(target_name, dns.rdatatype.MX)
    mxrecords = []
    for answers in response.answer:
        mx_name = answers.name
        for answer in answers:
            if answer.rdtype == 15:  # MX record
                mxrecords.append({"name": mx_name,
                                  "preference": answer.preference,
                                  "exchange": str(answer.exchange)})

    full_response["CNAME"] = cnames
    full_response["A"] = arecords
    full_response["AAAA"] = aaaarecords
    full_response["MX"] = mxrecords

    return full_response


def lookup(target_name: dns.name.Name,
           qtype: dns.rdata.Rdata) -> dns.message.Message:
    """
    This function uses a recursive resolver to find the relevant answer to the
    query.

    TODO: replace this implementation with one which asks the root servers
    and recurses to find the proper answer.
    """
    global total_calls_performed, canonical_resolution
    global whitelisted_cache, blacklisted_cache, tld_cache
    new_target_name = target_name
    if target_name in canonical_resolution:
        trgt = str(canonical_resolution[target_name])
        new_target_name = dns.name.from_text(trgt)
    index = 0

    while index < len(ROOT_SERVERS):
        faulty_canonical_found = False
        root = ROOT_SERVERS[index]
        if str(new_target_name)[str(new_target_name)[:-1].rfind(".")+1:] in tld_cache:
            root = tld_cache[str(new_target_name)[str(new_target_name)[:-1].rfind(".")+1:]]
        if root in blacklisted_cache:
            index += 1
            continue

        whitelisted_cache.clear()
        whitelisted_cache.add(root)
        response = recursive_resolver(new_target_name, qtype, root, root, 0)
        if response is None:
            blacklisted_cache.add(root)
        else:
            if len(response.answer) > 0:
                if qtype == dns.rdatatype.CNAME:
                    for answers in response.answer:
                        for answer in answers:
                            canonical_resolution[target_name] = answer
                else:
                    for answers in response.answer:
                        for answer in answers:
                            if answer.rdtype == dns.rdatatype.CNAME:
                                canonical_resolution[target_name] = answer
                                faulty_canonical_found = True
                    if faulty_canonical_found:
                        trgt = str(canonical_resolution[target_name])
                        new_target_name = dns.name.from_text(trgt)
                        continue
                break
            break
        index += 1

    # outbound_query = dns.message.make_query(target_name, qtype)
    # response = dns.query.udp(outbound_query, "8.8.8.8", 3)
    return response


@lru_cache(maxsize=1024)
def recursive_resolver(target_name: dns.name.Name,
                       qtype: dns.rdata.Rdata, root: str, ip: str, local_count: int):
    global total_calls_performed, canonical_resolution
    global whitelisted_cache, blacklisted_cache, tld_cache
    if ip in blacklisted_cache:
        return None
    total_calls_performed += 1
    try:
        outbound_query = dns.message.make_query(target_name, qtype)
        response: dns.message.Message = dns.query.udp(outbound_query, ip, 3)
        if len(response.answer) > 0:
            return response
        authority_list: dns.rrset.RRset.items = (response.authority[0]).items
        additional_list:  dns.rrset.RRset = response.additional

        authority_found = False
        authority_todo = []
        for authority in authority_list:
            authority_found = False
            for ad in additional_list:
                additional: dns.rrset.RRset = ad
                if additional.rdtype == 1:
                    inner_authority_found = False
                    for inner_authority in authority_list:
                        if str(additional.name) == str(inner_authority):
                            inner_authority_found = True
                            break
                    if inner_authority_found == False or str(additional.name) == str(authority):
                        authority_found = True
                        item: dns.rrset.RRset.items = additional.items[0]
                        output: dns.message.Message = \
                            recursive_resolver(target_name, qtype, root, str(item), local_count+1)
                        if output is not None:
                            if len(output.answer) > 0:
                                if local_count == 0:
                                    tld_cache [str((response.authority[0]).name)] = str(item)
                                return output
                            return response
                        break
            if not authority_found:
                authority_todo.append(str(authority))

        for index in authority_todo:
            addr_name = dns.name.from_text(index)
            resolution: dns.message.Message = dns_resolver(addr_name, 1, root)
            if resolution is not None:
                if len(resolution.answer) > 0:
                    for answers in resolution.answer:
                        a_name = answers.name
                        for answer in answers:
                            if answer.rdtype == 1:  # A record
                                output: dns.message.Message = \
                                    recursive_resolver(target_name, qtype, root,
                                                       str(answer), local_count+1)
                                if output is not None:
                                    if len(output.answer) > 0:
                                        return output

        return response
    except:
        blacklisted_cache.add(ip)
        return None


@lru_cache(maxsize=1024)
def dns_resolver(target_name: dns.name.Name, qtype: dns.rdata.Rdata, ip: str):
    global total_calls_performed, canonical_resolution
    global whitelisted_cache, blacklisted_cache
    total_calls_performed += 1
    if ip in blacklisted_cache:
        return None
    try:
        outbound_query = dns.message.make_query(target_name, qtype)
        response: dns.message.Message = dns.query.udp(outbound_query, ip, 3)
        if len(response.answer) > 0:
            return response
        for index in response.index.values():
            list_rrset: dns.rrset.RRset = index
            if list_rrset.rdtype == 1 and list_rrset.ttl > 0:
                list_inner: dns.rrset.RRset = list_rrset.items
                for i in list_inner:
                        output: dns.message.Message \
                            = dns_resolver(target_name, qtype, str(i))
                        if output is not None:
                            if len(output.answer) > 0:
                                return output
                            return response

        return response
    except:
        blacklisted_cache.add(ip)
        return None


def print_results(results: dict) -> None:
    """
    take the results of a `lookup` and print them to the screen like the host
    program would.
    """

    for rtype, fmt_str in FORMATS:
        for result in results.get(rtype, []):
            print(fmt_str.format(**result))


def main():
    """
    if run from the command line, take args and call
    printresults(lookup(hostname))
    """
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("name", nargs="+",
                                 help="DNS name(s) to look up")
    argument_parser.add_argument("-v", "--verbose",
                                 help="increase output verbosity",
                                 action="store_true")
    program_args = argument_parser.parse_args()
    for a_domain_name in program_args.name:
        print_results(collect_results(a_domain_name))


if __name__ == "__main__":
    main()
